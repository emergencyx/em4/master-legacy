//const debugStart = new Buffer('game_mode=3;inet=1;maxpl=4;nation=de;numpl=1;password=0;players=', 'ascii');
//const debugEnd = new Buffer(';server_addr=' + process.env.DEBUG_SERVER_ADDR + ';server_name=' + process.env.DEBUG_SERVER_NAME + ';server_port=58282;vmaj=1;vmin=0;vtype=f', 'ascii');
//const debugEnd = new Buffer(';server_addr=192.168.1.25;server_name=EMX (noBlubb);server_port=58282;vmaj=1;vmin=0;vtype=f', 'ascii');

//const debugStart2 = new Buffer('game_mode=1;maxpl=0;nation=de;numpl=0;password=0;players=', 'ascii');
//const debugEnd2 = new Buffer(';server_name=' + process.env.DEBUG_SERVER_NAME + ';server_port=58282;vmaj=1;vmin=0;vtype=f', 'ascii');

//const dbgPlayers = formulatePlayerString([process.env.DEBUG_SERVER_USER]);
//const dbgServer = Buffer.concat([debugStart2, dbgPlayers, debugEnd2]);
//TODO(rs) 29,30,31 -> ascii record control characters, might explain the "magic"

const playerStart = Buffer.of(0xa7, 0x28, 0x31, 0x30, 0x29, 0x1b);
const playerEnd = Buffer.of(0x1c, 0xa7, 0x28, 0x2d, 0x31, 0x29, 0x1b);
const playerSeparatorString = ', ';
const playerSeparator = Buffer.from(', ');

const tupleSeparator = Buffer.from(';');
const valueSeparator = Buffer.from('=');

/**
 * This is very likely the wire format used for messages.
 * Some special characters are allowed (ö, ä, ü), but it is not utf-8.
 */
const encoding = 'win1252';

module.exports = {
    playerStart,
    playerEnd,
    playerSeparatorString,
    playerSeparator,
    tupleSeparator,
    valueSeparator,
    encoding
};
