const http = require('http');
const WebSocket = require('ws');
const filterPublic = require('../util/serializer').filterPublic;
const express = require('express');
const basicAuth = require('express-basic-auth')
const logger = require('../util/log');

const factory = (repository) => {
    function serialize(sessions) {
        return sessions.map((sessions) => filterPublic(sessions));
    }

    const app = express();
    app.use(express.json());

    app.get('/api/sessions', (req, res) => {
        res.json(serialize(repository.getSessions()));
        res.end();
    });

    app.get('/api/health', (req, res) => {
        res.end('🚀');
    });

    const server = http.createServer(app);
    const webSocket = new WebSocket.Server({ server, path: '/websocket' });

    webSocket.on('connection', (ws) => {
        ws.send(JSON.stringify(serialize(repository.getSessions())));
    });

    repository.on('update', (sessions) => {
        const message = JSON.stringify(serialize(sessions));
        webSocket.clients.forEach((client) => {
            if (client.readyState === WebSocket.OPEN) {
                client.send(message);
            }
        });
    });

    app.use(basicAuth({
        users: { 'modem': process.env.APP_TOKEN }
    }));

    app.post('/api/modem', (req, res) => {
        if (req.body && req.body.id && req.body.modem) {
            if (repository.update({ modem: req.body.modem }, req.body.id)) {
                logger.info(`http::modem Enable modem for ${req.body.id} on ${req.body.modem.ip}:${req.body.modem.port}`, req.body);
                res.status(200).end();
            } else {
                res.status(410).end();
            }

            return;
        }
        res.status(400).end();
    });

    return server;
};

module.exports = factory;
