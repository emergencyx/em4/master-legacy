const decode = require('iconv-lite').decode;
const magic = require('../magic');
const encoding = magic.encoding;
const logger = require('../util/log');

const typeMapper = {
    alive: 'bool',
    password: 'bool',
    game: 'string',
    lang: 'string',
    mod: 'string',
    players: 'players',
    game_mode: 'int',
    maxpl: 'int',
    numpl: 'int',
    server_port: 'int',
    vmaj: 'int',
    vmin: 'int',
    vtype: 'string',
    nation: 'string',
    server_name: 'string'
};

function request(message) {
    let index = 0;

    const object = {};
    for (let boundedLoop = 0; boundedLoop < 20; boundedLoop++) {
        const newIndex = message.indexOf(magic.tupleSeparator, index);
        const part = newIndex === -1 ? message.slice(index) : message.slice(index, newIndex);

        const split = part.indexOf(magic.valueSeparator);
        const key = part.slice(0, split).toString();
        const value = part.slice(split + 1);

        switch (typeMapper[key]) {
            case 'string':
                object[key] = decode(value, encoding);
                break;
            case 'players':
                object[key] = decodePlayers(value);
                break;
            case 'bool':
                object[key] = decode(value, encoding) === '1';
                break;
            case 'int':
                object[key] = Number.parseInt(decode(value, encoding));
                break;
            default:
                logger.error(`Unknown key ${key} with value ${value}`);
        }

        if (newIndex === -1) {
            break;
        }

        index = newIndex + 1;
    }

    return object;
}

function decodePlayers(value) {
    let index = 0;

    const players = [];
    for (let boundedLoop = 0; boundedLoop < 20; boundedLoop++) {
        let newIndex = value.indexOf(magic.playerSeparator, index);
        let part = newIndex === -1 ? value.slice(index) : value.slice(index, newIndex);

        if (part.indexOf(magic.playerStart) === 0) {
            part = part.slice(magic.playerStart.length);
        }
        if (part.includes(magic.playerEnd)) {
            part = part.slice(0, part.indexOf(magic.playerEnd));
        }

        players.push(decode(part, encoding));

        if (newIndex === -1) {
            break;
        }

        index = newIndex + magic.playerSeparator.length;
    }

    return players;
}

module.exports = {
    request,
    decodePlayers
};
