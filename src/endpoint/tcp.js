const net = require('net');
const uuid = require('uuid/v4');
const Repository = require('../repository');
const parser = require('../util/parser');
const logger = require('../util/log');

class TcpEndpoint {
    /**
     * 
     * @param {Repository} repository 
     */
    constructor(repository) {
        this.tcpHost = this.createTcpHost();
        this.repository = repository;
    }

    createTcpHost() {
        const server = net.createServer((socket) => {
            if (!socket.remoteAddress) {
                logger.error('tcp::connect remoteAddress is empty');
                socket.destroy();
                return;
            }

            socket.id = uuid();
            logger.debug('tcp::connect id:%s remote:%s', socket.id, socket.remoteAddress);
            this.repository.create({ server_addr: socket.remoteAddress, inet: true }, socket.id);

            socket.on('data', (data) => {
                logger.debug('tcp::data id:%s data:%s', socket.id, data.toString('hex'));

                let offset = 0;
                while (offset + 12 < data.byteLength) {
                    const id = data.slice(offset, offset + 8);
                    const length = data.readUInt32LE(offset + 8);
                    offset += 12;
                    if (offset + length <= data.byteLength) {
                        this.onMessage(socket.id, data.slice(offset, offset + length));
                    }
                    offset += length;
                }
            });

            socket.on('error', (error) => {
                logger.error(error);
            });

            socket.on('end', () => {
                this.repository.update({ run: true }, socket.id);
                logger.debug('tcp::disconnect id:%s', socket.id);
            });
        });

        server.on('listening', () => {
            const address = server.address();
            logger.info(`tcp::up ${address.address}:${address.port}`);
        });
        return server;
    }

    onMessage(id, message) {
        const values = parser.request(message);
        this.repository.update(values, id);
    }
}

module.exports = TcpEndpoint;
