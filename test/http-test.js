const Repository = require('../src/repository');

const assert = require('assert');
const chai = require('chai');
chai.use(require('chai-http'));
const expect = chai.expect;

describe('HTTP API', function () {
    let app;
    let repository;
    let requester;

    before(function () {
        repository = new Repository([{
            server_name: 'test',
            nation: 'de',
            mod: '',
            players: ['noBlubb', 'ciajoe'],
            password: false,
            run: false,
            maxpl: 4,
            numpl: 2,
            id: 'test-id'
        }]);
        app = require('../src/endpoint/http')(repository);
        requester = chai.request(app).keepOpen();
        assert.ok(requester, 'Test server not initialized');
    });

    it('should return running sessions', function (done) {
        requester
            .get('/api/sessions')
            .end(function (err, res) {
                expect(res).to.be.json;
                expect(res).to.have.status(200);
                expect(res.body).to.be.deep.equal([{
                    id: 'test-id',
                    name: 'test', players: ['noBlubb', 'ciajoe'],
                    nation: 'de',
                    mod: '',
                    password: false,
                    started: false,
                    maxpl: 4,
                    numpl: 2,
                    connectivity: false,
                    modem: false
                }]);
                done();
            });
    });

    it('should return friendly names', function (done) {
        repository.create({
            id: 'friendly-test',
            server_name: Buffer.from('a3JpZWc=', 'base64').toString(),
            players: [],
            nation: 'de',
            mod: '',
            password: false,
            started: false,
            maxpl: 4,
            numpl: 2,
            connectivity: true
        }, 'friendly-test');

        requester
            .get('/api/sessions')
            .end(function (err, res) {
                expect(res).to.be.json;
                expect(res).to.have.status(200);
                expect(res.body).to.deep.include({
                    id: 'friendly-test',
                    name: '♡♡♡♡♡',
                    players: [],
                    nation: 'de',
                    mod: '',
                    password: false,
                    started: false,
                    maxpl: 4,
                    numpl: 2,
                    connectivity: true,
                    modem: false
                });
                done();
            });
    });

    it('should return modem status', function (done) {
        repository.create({
            id: 'modem-test',
            server_name: 'Modem Test',
            players: [],
            nation: 'de',
            mod: '',
            password: false,
            started: false,
            maxpl: 4,
            numpl: 2,
            connectivity: true,
            modem: { ip: '127.0.0.1', port: 58282 }
        }, 'modem-test');

        requester
            .get('/api/sessions')
            .end(function (err, res) {
                expect(res).to.be.json;
                expect(res).to.have.status(200);
                expect(res.body).to.deep.include({
                    id: 'modem-test',
                    name: 'Modem Test',
                    players: [],
                    nation: 'de',
                    mod: '',
                    password: false,
                    started: false,
                    maxpl: 4,
                    numpl: 2,
                    connectivity: true,
                    modem: true
                });
                done();
            });
    });


    after(function () {
        requester.close();
        clearInterval(repository.timeout);
    });
});
