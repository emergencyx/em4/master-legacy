const dgram = require('dgram');
const request = require('../util/parser').request;
const serialize = require('../util/serializer').session;
const logger = require('../util/log');
const applyModem = require('../util/modem').applyModem;

/*
 <var name="master_server_port" value="54321" />
 <var name="match_making_port" value="12345" />

 MasterHost: serves the session list
 */

class UdpEndpoint {
    constructor(repository) {
        this.udpHost = this.createMasterHost(); //Endpoint used for list communication
        this.repository = repository;
    };

    createMasterHost() {
        const socket = dgram.createSocket('udp4');

        socket.on('error', (error) => {
            logger.error(error);
        });

        socket.on('message', (msg, rinfo) => {
            if (msg.length < 4) {
                logger.warn('Received empty udp message', { msg, rinfo });
                return;
            }

            const id = msg.readUInt16LE(0);
            const type = msg.readUInt8(2);
            const length = msg.readUInt8(3);

            if (length + 4 <= msg.byteLength) {
                this.handleMasterRequest(id, msg.slice(4, 4 + length), rinfo);
            }
        });

        socket.on('listening', () => {
            const address = socket.address();
            logger.info(`udp::up ${address.address}:${address.port}`);
        });

        return socket;
    }

    /**
     * Handle (parse and split) a master server request
     * @param {Number} id
     * @param {Buffer} message
     * @param {Socket} client
     * @returns {undefined}
     */
    handleMasterRequest(id, message, client) {
        const data = request(message);
        if ('alive' in data) {
            this.repository.alive(client.address, data.alive);
        } else {
            this.onListQuery(id, data, client);
        }
    }

    /**
     * ListRequest handler
     */
    onListQuery(id, query, client) {
        let sessions = this.repository.getSessions();
        if (query.mod) {
            sessions = sessions.filter(session => session.mod === query.mod);
        }

        sessions.forEach(session => {
            const body = serialize(applyModem(session));
            const message = this.wrap(id, 0x80, body);
            if (message) {
                this.udpHost.send(message, client.port, client.address);
            }
        });
    }

    wrap(id, type, body) {
        const length = body.length;
        if (length > 255) {
            logger.error('Body too large - omitting session from udp', { id, type, body });
            return;
        }

        const header = Buffer.alloc(4);
        header.writeUInt16LE(id, 0, 2);
        header.writeUInt8(type, 2, 1);
        header.writeUInt8(length, 3, 1);

        return Buffer.concat([header, body]);
    }

    /**
     * Handle udp messages
     *
     * ListRequest game=EM4 lang=de [mod=BFEMP]
     * AliveRequest game=EM4 alive=1
     */
}

module.exports = UdpEndpoint;
