const encode = require('iconv-lite').encode;
const magic = require('../magic');
const filter = require('./friendly').filter;

const typeMapper = {
    'inet': 'bool',
    'run': 'bool',
    'password': 'bool',
    'mod': 'string',
    'players': 'players',
    'game_mode': 'int',
    'maxpl': 'int',
    'numpl': 'int',
    'server_addr': 'string',
    'server_port': 'int',
    'vmaj': 'int',
    'vmin': 'int',
    'vtype': 'string',
    'nation': 'string',
    'server_name': 'string'
};

//game_mode=3;maxpl=2;mod=BFEMP2020;nation=us;numpl=2;password=1;players=�(10)\u001bFabian\u001c�(-1)\u001b, Eddy;server_name=FF80 (Fabian);server_port=58282;vmaj=1;vmin=3;vtype=f'

function formatTimestamp(millis) {
    const date = new Date(millis);
    return date.toISOString().slice(0,-5)+"Z"
}

/**
 * Serialize a player array and return a formatted version
 * @param {Array<string>} players
 * @returns {Buffer} serialized players
 */
function players(players) {
    if (!players.length) {
        throw new Error('Can not serialize empty player array');
    }

    // First player
    const firstPlayer = players[0];
    if (firstPlayer) {
        const serialize = [magic.playerStart, encode(firstPlayer, magic.encoding), magic.playerEnd];

        // Other players
        for (let i = 1; i < players.length; i++) {
            serialize.push(magic.playerSeparator);
            serialize.push(encode(players[i], magic.encoding));
        }

        return Buffer.concat(serialize);
    }

    return Buffer.alloc(0);
}

function session(session) {
    return Buffer.concat(
        Object.keys(session)
            .filter(field => typeMapper.hasOwnProperty(field))
            .reduce((accumulator, field, index, all) => {
                accumulator.push(encode(field, magic.encoding));
                accumulator.push(magic.valueSeparator);

                switch (typeMapper[field]) {
                    case 'bool':
                        accumulator.push(Buffer.from(session[field] ? '1' : '0'));
                        break;
                    case 'string':
                        accumulator.push(encode(session[field], magic.encoding));
                        break;
                    case 'int':
                        accumulator.push(Buffer.from(session[field].toString()));
                        break;
                    case 'players':
                        accumulator.push(players(session[field]));
                        break;
                }

                if (index < all.length - 1) {
                    accumulator.push(magic.tupleSeparator);
                }

                return accumulator;
            }, [])
    );
}

/**
 * Prepares a session for the session list.
 * @param {Object} session 
 * @returns Object
 */
function filterPublic(session) {
    return {
        name: session.server_name ? filter(session.server_name) : 'Unbekannter Server',
        players: session.players ? session.players.map(filter) : [],
        numpl: session.numpl || 0,
        maxpl: session.maxpl || 0,
        mod: session.mod || '',
        nation: session.nation || '',
        started: session.run || false,
        password: session.password || false,
        connectivity: session.connectivity || false,
        id: session.id || '',
        modem: !!session.modem
    }
}

/**
 * Prepares a session to be transferred to the history service.
 * @param {Object} session 
 * @returns Object
 */
function filterHistory(session) {
    return {
        name: session.server_name ? filter(session.server_name) : 'Unbekannter Server',
        numpl: session.numpl || 0,
        maxpl: session.maxpl || 0,
        mod: session.mod || '',
        nation: session.nation || '',
        started: session.run || false,
        password: session.password || false,
        connectivity: session.connectivity || false,
        id: session.id || '',
        modem: !!session.modem,
        created_at: session.created_at ? formatTimestamp(session.created_at) : '',
        ended_at: session.ended_at ? formatTimestamp(session.ended_at): '',
    }
}

module.exports = {
    players,
    session,
    filterPublic,
    filterHistory
};
