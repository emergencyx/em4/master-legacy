const dgram = require('dgram');
const logger = require('../util/log');

class ConnectivityEndpoint {
    constructor(repository) {
        this.udpHost = this.createMasterHost();
        this.repository = repository;
        this.interval = null;
    };

    createMasterHost() {
        const socket = dgram.createSocket('udp4');

        socket.on('error', (error) => {
            logger.error(error);
        });

        socket.on('close', () => {
            if (this.interval) {
                clearInterval(this.interval);
                this.interval = null;
            }
            logger.info(`connectivity::down - clearing interval`);
        });

        socket.on('message', (message, rinfo) => {
            this.onGameResponse(message, rinfo);
        });

        socket.on('listening', () => {
            const address = socket.address();
            logger.info(`connectivity::up ${address.address}:${address.port}`);

            const request = Buffer.from('35ee000867616d653d454d34', 'hex');
            this.interval = setInterval(() => {
                const sessions = this.repository.getSessions();
                for (const session of sessions) {
                    if (session.server_addr && session.server_name) {
                        logger.info(`Querying ${session.server_name} on ${session.server_addr}:12345...`, { session });
                        socket.send(request, 12345, session.server_addr);
                    }
                }
            }, 15000);
        });

        return socket;
    }

    onGameResponse(msg, rinfo) {
        logger.warn(`connectivity::message ${msg.toString('hex')} from ${rinfo.address}:${rinfo.port}`, { msg, rinfo });
        this.repository.connectivity(rinfo.address);
    }
}

module.exports = ConnectivityEndpoint;
