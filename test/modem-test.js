const dgram = require('dgram');
const expect = require('chai').expect;
const applyModem = require('../src/util/modem').applyModem;

describe('Modem interations', function () {
    it('should overwrite with modem information if present', function () {
        const session = applyModem({
            run: true,
            server_addr: '127.0.0.1',
            inet: true,
            game_mode: 3,
            maxpl: 2,
            mod: 'BFEMP',
            nation: 'de',
            numpl: 2,
            password: false,
            players: [
                'noBlubb',
                'ciajoe'
            ],
            server_name: 'EmergencyX (noBlubb)',
            server_port: 58282,
            vmaj: 1,
            vmin: 3,
            vtype: 'f',
            modem: {
                ip: '10.0.0.0',
                port: 50001
            }
        });

        expect(session.server_addr).to.equal('10.0.0.0');
        expect(session.server_port).to.equal(50001);
    });

    it('should otherwise not modify the session', function () {
        const originalSession = {
            run: true,
            server_addr: '127.0.0.1',
            inet: true,
            game_mode: 3,
            maxpl: 2,
            mod: 'BFEMP',
            nation: 'de',
            numpl: 2,
            password: false,
            players: [
                'noBlubb',
                'ciajoe'
            ],
            server_name: 'EmergencyX (noBlubb)',
            server_port: 58282,
            vmaj: 1,
            vmin: 3,
            vtype: 'f',
        };
        const session = applyModem(originalSession);
        expect(session).to.equal(originalSession);
    });
});
