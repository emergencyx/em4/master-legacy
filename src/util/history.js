const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file');

const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format((info, opts) => {
            return info.history ? info : false;
        })(),
        winston.format.timestamp(),
        winston.format.json()
    ),
    transports: [
        new DailyRotateFile({
            dirname: 'history',
            extension: '.json',
            utc: true,
        })
    ]
});

function sessionStarted(session) {
    logger.info('session::started', { session, history: true });
}

function sessionEnded(session) {
    logger.info('session::ended', { session });
}

module.exports = {
    sessionStarted,
    sessionEnded
};
